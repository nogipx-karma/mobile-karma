import 'package:flutter/material.dart';
import 'dart:math';

mixin SinusoidBezier {

  List<Offset> bezierPivotPoints({
    required Offset begin,
    required Offset end,
    required Offset control,
    int periods = 1
  }) {
    final periodsAll = periods * 2 + 1;
    final step = 1 / (periodsAll-1);
    final List<Offset> pivotPoints = 
      List<double>.generate(
        periodsAll, (index) => step * index
      ).map((t) => bezier(
        begin: begin, end: end,
        time: t, control: control
      )).toList();
    
    return pivotPoints;
  }

  Offset bezier({
    required Offset begin, 
    required Offset end,
    required Offset control,
    required double time
  }) {
    assert(time >= 0.0 && time <= 1.0);
    final startModifier = pow((1 - time), 2);
    final startOffset = Offset(
      begin.dx * startModifier, 
      begin.dy * startModifier
    );

    final controlModifier = 2 * time * (1 - time);
    final controlOffset = Offset(
      control.dx * controlModifier, 
      control.dy * controlModifier
    );

    final endModifier = pow(time, 2);
    final endOffset = Offset(
      end.dx * endModifier,
      end.dy * endModifier
    );

    return startOffset + controlOffset + endOffset; 
  }


  Offset lineCenter(Offset begin, Offset end) {
    final size = Size(
      (begin.dx - end.dx).abs(),
      (begin.dy - end.dy).abs()
    );
    return Offset(
      begin.dx < end.dx ? begin.dx + size.width / 2 : begin.dx - size.width / 2,
      begin.dy < end.dy ? begin.dy + size.height / 2 : begin.dy - size.height / 2
    );
  }

  double perpendicularLine(Offset begin, Offset end, double x) {
    // A & B of initial equation
    final a = begin.dy - end.dy;
    final b = end.dx - begin.dx;
    // Normal vector
    final n = Offset(a, b);
    // Point on line
    final p = lineCenter(begin, end);

    return (x * n.dy - p.dx * n.dy + p.dy * n.dx) / n.dx;
  }

  Offset controlWithNormal({
    required Offset begin, 
    required Offset end,
    required double curveModifier,
    bool upside = true
  }) {
    // a & b coefficients of line equation
    final a = begin.dy - end.dy;
    final b = end.dx - begin.dx;
    final normal = Offset(a, b) * curveModifier * sqrt(1 / (pow(a, 2) + pow(b, 2)));
    final center = lineCenter(begin, end);

    return upside ? center - normal : center + normal;
  }
}