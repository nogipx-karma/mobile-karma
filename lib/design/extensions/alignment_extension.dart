import 'package:flutter/foundation.dart';
import 'package:flutter/rendering.dart';

extension ScreenAlignmentExt on Alignment {
  Alignment diagonal({required Color color, required  double width}) {
    if (this == Alignment.topLeft)
      return Alignment.bottomRight;

    else if (this == Alignment.topRight)
      return Alignment.bottomLeft;

    else if (this == Alignment.bottomRight)
      return Alignment.topLeft;

    else if (this == Alignment.bottomLeft)
      return Alignment.topRight;

    else throw UnimplementedError();
  }

  BorderRadius toCornerBorderRadius({required Radius radius}) {
    if (this == Alignment.topLeft)
      return BorderRadius.only(bottomRight: radius);

    else if (this == Alignment.topRight)
      return BorderRadius.only(bottomLeft: radius);

    else if (this == Alignment.bottomRight)
      return BorderRadius.only(topLeft: radius);

    else if (this == Alignment.bottomLeft)
      return BorderRadius.only(topRight: radius);

    else if (this == Alignment.topCenter)
      return BorderRadius.only(bottomLeft: radius, bottomRight: radius);

    else if (this == Alignment.bottomCenter)
      return BorderRadius.only(topLeft: radius, topRight: radius);

    else if (this == Alignment.centerLeft)
      return BorderRadius.only(topRight: radius, bottomRight: radius);

    else if (this == Alignment.centerRight)
      return BorderRadius.only(bottomLeft: radius, topLeft: radius);

    else return BorderRadius.all(radius);
      
  }

  bool get isSideHorizontal =>
    this == Alignment.topCenter || this == Alignment.bottomCenter;

  bool get isSideVertical =>
    this == Alignment.centerLeft || this == Alignment.centerRight;

  bool get isCorner =>
    this == Alignment.topLeft || this == Alignment.topRight ||
    this == Alignment.bottomLeft || this == Alignment.bottomRight;
}