import 'dart:ui';

import 'package:flutter/material.dart';


class AppColor {

  static const eerieBlack = Color(0xff212121);
  static const jet = Color(0xff323232);
  static const flame = Color(0xffF15025);
  static const platinum = Color(0xffE6E8E6);
  static const onyx = Color(0xff3C3F41);
  static const darkPastelRed = Color(0xffC6421F);
  static const red = Color(0xffff0000);
  static const richBlack = Color(0xff000000);
  static const mintCream = Color(0xffF7FFF7);
  static const prussianBlue = Color(0xff273043);
  static const imperialRed = Color(0xffec2834);

  static const mediumStaleBlue = Color(0xff736CED);
  static const lavenderFloral = Color(0xffC287E8);
  static const mellowApricot = Color(0xffF9B16E);
  static const lightCoral = Color(0xffF27A7D);
  static const papayaWhip = Color(0xffFAEDCB);

}


class AppAsset {

  static String googleAsset(id) => "https://drive.google.com/uc?export=view&id=$id";

  static final bio_avatar = googleAsset("1AaLPcELhbmvvqu2GWBz8HYMzSs8qUj8k");
  static final bio_bg = googleAsset("11DFXqUuQm4RffIPLsppshqGp0Df0Pl4N");

}


class MydnessText {

  static const about_me =
    "Привет.\n"
    "Как ты? Как жизнь? Надеюсь всё хорошо 😉\n"
    "Ты попал, знаешь ли... На страницу обо мне";

  static const welcome_title = "HI, I'M KARIM";
  static const welcome_subtitle = "Mobile Developer from Russia 🇷🇺";

}