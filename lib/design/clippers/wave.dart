import 'dart:math';

import 'package:flutter/material.dart';
import 'package:karma/design/extensions/bezier_mixin.dart';


class _SinusoidPartSetting {
  final Offset begin;
  final Offset end;

  Offset? control;

  _SinusoidPartSetting(this.begin, this.end);
  @override
  String toString() => "Begin { $begin }; End { $end }; Control { $control }";
}

class WaveClipper extends CustomClipper<Path> with SinusoidBezier {
  final Offset begin;
  final Offset end;
  final int periods;
  final double mainCurveMod;
  final double partCurveMod;
  final bool isConvexUp;

  WaveClipper({
    required this.begin, 
    required this.end, 
    this.periods = 1,
    this.isConvexUp = true,
    this.mainCurveMod = 1.0, 
    this.partCurveMod = 1.0, 
  });

  @override
  Path getClip(Size size) {
    final mainControl = controlWithNormal(
      begin: begin,
      end: end,
      curveModifier: mainCurveMod,
      upside: isConvexUp
    );
    final pivots = bezierPivotPoints(
      begin: begin,
      end: end,
      periods: periods,
      control: mainControl
    );

    List<_SinusoidPartSetting> sinusoidParts = [];
    
    pivots.reduce((current, next) {
      sinusoidParts.add(_SinusoidPartSetting(
        current, next
      ));
      return next;
    });

    sinusoidParts = sinusoidParts.asMap().entries.map((e) {
      e.value.control = controlWithNormal(
        begin: e.value.begin,
        end: e.value.end,
        curveModifier: partCurveMod,
        upside: e.key.isEven
      );
      return e.value;
    }).toList();

    sinusoidParts.forEach(print);
    var path = Path();

    sinusoidParts.forEach((part) {
      path.moveTo(part.begin.dx, part.begin.dy);
      path.quadraticBezierTo(
        part.control!.dx, part.control!.dy,
        part.end.dx, part.end.dy, 
      );
      // path.addOval(Rect.fromCenter(center: part.control, width: 10, height: 10));
    });

    // path.lineTo(end.dx, size.height);
    // path.lineTo(0, size.height);
    // path.lineTo(0, begin.dy);
    // path.quadraticBezierTo(mainControl.dx, mainControl.dy, end.dx, end.dy);

    return path;
  }


  @override
  bool shouldReclip(covariant CustomClipper<Path> oldClipper) => this != oldClipper;

}