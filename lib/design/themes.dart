import 'package:flutter/material.dart';
import 'package:karma/design/export.dart';
import 'package:flutter_google_ui/flutter_google_ui.dart';
import 'package:google_fonts/google_fonts.dart';

class AppThemes {

  static final darkTheme = ThemeData(
    primaryColor: AppColor.jet,
    backgroundColor: AppColor.jet,
    scaffoldBackgroundColor: AppColor.mintCream,
    accentColor: AppColor.flame,
    hoverColor: AppColor.flame.hoverDark,
    splashColor: AppColor.flame.pressedDark,
    disabledColor: AppColor.platinum,
    highlightColor: AppColor.flame.activatedDark,
    textTheme: GoogleFonts.robotoSlabTextTheme().apply(
      bodyColor: AppColor.jet,
      displayColor: AppColor.jet
    ),
    primaryTextTheme: GoogleFonts.londrinaSketchTextTheme().apply(
      bodyColor: AppColor.flame,
      displayColor: AppColor.flame,
    ),
    accentTextTheme: GoogleFonts.alegreyaSansTextTheme().apply(
      bodyColor: AppColor.jet,
      displayColor: AppColor.jet,
    )
  );

}