import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

class OutsideLink {
  final String name;
  final Color? color;
  final String? link;
  final IconData? icon;

  OutsideLink({
    required this.name,
    this.link,
    this.color,
    this.icon,
  });
}

class MySocials {

  static final instagram = OutsideLink(
    name: "Instagram",
    link: "https://instagram.com/nogipx",
    color: Color(0xffe4405f),
    icon: FontAwesomeIcons.instagram
  );

  static final ya_radio = OutsideLink(
    name: "Ya.Radio",
    link: "https://radio.yandex.ru/user/nogipx",
    color: Color(0xfff5cc03),
    icon: FontAwesomeIcons.yandexInternational
  );

  static final gitlab = OutsideLink(
    name: "Gitlab",
    link: "https://gitlab.com/nogipx",
    color: Color(0xffe2432a),
    icon: FontAwesomeIcons.gitlab
  );

  static final stackoverflow = OutsideLink(
    name: "StackOverflow",
    link: "https://stackoverflow.com/users/11475298/nogipx",
    color: Color(0xffef8024),
    icon: FontAwesomeIcons.stackOverflow
  );

  static final telegram = OutsideLink(
    name: "Telegram",
    link: "https://t.me/nogipx",
    color: Color(0xff0088cc),
    icon: FontAwesomeIcons.telegramPlane
  );

  static final active = [gitlab, instagram, telegram, ya_radio, stackoverflow];
}


class WorkRelated {

  static final hh = OutsideLink(
    name: "HeadHunter резюме",
    link: "https://sochi.hh.ru/resume/d0540153ff07434ee90039ed1f596d31537552",
    color: Color(0xffd6241a)
  );

  static final stepik = OutsideLink(
    name: "Stepik - курсы",
    link: "https://stepik.org/users/52590992",
    color: Color(0xff66cb66)
  );

  static final codewars = OutsideLink(
    name: "Codewars - задачи по программированию",
    link: "https://www.codewars.com/users/nogipx",
    color: Color(0xffbb432c)
  );

  static final hackerrank = OutsideLink(
    name: "HakerRank - задачи по программированию",
    link: "https://www.codewars.com/users/nogipx",
    color: Color(0xff51c765),
    icon: FontAwesomeIcons.hackerrank
  );

  static final linkedin = OutsideLink(
    name: "LinkedIn - сейчас не используется",
    link: "https://www.linkedin.com/in/karim-mamatkazin-3635b6193",
    color: Color(0xff0e76a8),
    icon: FontAwesomeIcons.linkedinIn
  );

  static final active = [hh, codewars, hackerrank, stepik, linkedin];
}