import 'outside_link.dart';

enum ExperinceType {
  FullTime, PartTime, PetProject
}

class Experience {
  final String name;
  final String? description;
  final String positionName;
  final String? positionDesc;
  final DateTime from;
  final DateTime? to;
  final Uri? logo;
  final Uri? site;
  final Uri? googlePlay;
  final Uri? appStore;
  final ExperinceType type;
  final List<Experience>? products;
  final List<OutsideLink>? languages;
  final List<OutsideLink>? tools;

  Experience({
    required this.name, 
    required this.positionName, 
    required this.from, 
    required this.type,
    this.description, 
    this.to, 
    this.googlePlay,
    this.appStore,
    this.tools, 
    this.languages,
    this.products, 
    this.positionDesc,
    this.logo, 
    this.site, 
  });
}