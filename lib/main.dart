import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:karma/design/export.dart';
import 'package:loggable_mixin/loggable_mixin.dart';

void main() async {
  runApp(
    MultiRepositoryProvider(
      providers: [],
      child: MultiBlocProvider(
        providers: [],
        child: App(),
      ),
    ),
  );
}

class App extends StatelessWidget with Loggable {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: "Mydness",
      theme: AppThemes.darkTheme,
      home: Container(),
    );
  }
}
