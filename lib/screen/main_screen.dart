import 'package:flutter/material.dart';
import 'package:karma/design/colors.dart';
import 'package:karma/design/extensions/bezier_mixin.dart';

class MainScreen extends StatefulWidget {
  @override
  _MainScreenState createState() => _MainScreenState();
}

class _MainScreenState extends State<MainScreen> with SingleTickerProviderStateMixin, SinusoidBezier {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        color: AppColor.darkPastelRed,
      ),
    );
  }
}
