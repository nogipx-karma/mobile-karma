import 'package:flutter/material.dart';

class TransformingDrawer extends StatefulWidget {
  final Widget child;
  final Widget body;
  final Widget? background;
  final Widget? foreground;
  final AnimationController? animCtrl;
  final double childBorderWidth;
  final Color childBorderColor;
  final Color? scaffoldColor;

  const TransformingDrawer({
    Key? key,
    required this.child,
    required this.body,
    this.animCtrl,
    this.background,
    this.foreground,
    this.childBorderWidth = 4,
    this.scaffoldColor,
    this.childBorderColor = Colors.black,
  }) : super(key: key);

  @override
  _TransformingDrawerState createState() => _TransformingDrawerState();
}

class _TransformingDrawerState extends State<TransformingDrawer> with SingleTickerProviderStateMixin {
  late AnimationController _animationController;
  bool _canDrag = false;

  static const Duration toggleDuration = Duration(milliseconds: 250);
  static const double maxSlide = 200;
  static const double minDragStartEdge = 60;
  static const double maxDragStartEdge = maxSlide - 16;

  @override
  void initState() {
    _animationController = widget.animCtrl ?? AnimationController(vsync: this, duration: toggleDuration);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: widget.scaffoldColor,
      body: GestureDetector(
        onTap: () => toggle(),
        onHorizontalDragEnd: _onDragEnd,
        onHorizontalDragStart: _onDragStart,
        onHorizontalDragUpdate: _onDragUpdate,
        child: AnimatedBuilder(
          animation: _animationController,
          builder: (context, _) {
            return Stack(
              children: [
                Center(
                  child: Transform(
                    transform: Matrix4.identity()
                      ..translate(0.0, -60)
                      ..translate(
                        0.0,
                        60 * _animationController.value,
                      ),
                    child: Opacity(
                      opacity: _animationController.value,
                      child: widget.background ?? SizedBox(),
                    ),
                  ),
                ),
                Transform(
                  transform: Matrix4.identity()
                    ..translate(maxSlide * _animationController.value)
                    ..scale(1 - (0.3 * _animationController.value))
                    ..setEntry(3, 2, 0.001)
                    ..rotateY(3.14 / 8 * _animationController.value),
                  alignment: Alignment.centerLeft,
                  child: Container(
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(20 * _animationController.value),
                      border: Border.all(
                        color: widget.childBorderColor,
                        width: widget.childBorderWidth * _animationController.value,
                      ),
                    ),
                    child: ClipRRect(
                      borderRadius: BorderRadius.circular(16 * _animationController.value),
                      child: widget.child,
                    ),
                  ),
                ),
                Transform(
                  transform: Matrix4.identity()
                    ..translate(
                      0.0,
                      60 * (1 - _animationController.value),
                    ),
                  child: Opacity(
                    opacity: _animationController.value,
                    child: widget.foreground ?? SizedBox(),
                  ),
                ),
                Transform(
                  alignment: Alignment.centerLeft,
                  transform: Matrix4.identity()
                    ..translate(maxSlide * (1 - _animationController.value) * -1)
                    ..scale(1 - (0.1 * _animationController.value)),
                  child: Container(
                    width: maxSlide,
                    child: Align(
                      alignment: Alignment.centerLeft,
                      child: Material(
                        type: MaterialType.transparency,
                        child: widget.body,
                      ),
                    ),
                  ),
                ),
              ],
            );
          },
        ),
      ),
    );
  }

  @override
  void dispose() {
    _animationController.dispose();
    super.dispose();
  }

  void open() => _animationController.forward();
  void close() => _animationController.reverse();

  void toggle() => _animationController.isDismissed ? open() : close();

  void _onDragStart(DragStartDetails details) {
    bool isDragOpenFromLeft = _animationController.isDismissed && details.globalPosition.dx < minDragStartEdge;
    bool isDragCloseFromRight = _animationController.isCompleted && details.globalPosition.dx > maxDragStartEdge;

    _canDrag = isDragOpenFromLeft || isDragCloseFromRight;
  }

  void _onDragUpdate(DragUpdateDetails details) {
    if (_canDrag) {
      double delta = details.primaryDelta! / maxSlide;
      _animationController.value += delta;
    }
  }

  void _onDragEnd(DragEndDetails details) {
    double _kMinFlingVelocity = 365.0;

    if (_animationController.isDismissed || _animationController.isCompleted) {
      return;
    }
    if (details.velocity.pixelsPerSecond.dx.abs() >= _kMinFlingVelocity) {
      double visualVelocity = details.velocity.pixelsPerSecond.dx / MediaQuery.of(context)!.size.width;

      _animationController.fling(velocity: visualVelocity);
    } else if (_animationController.value < 0.5) {
      close();
    } else {
      open();
    }
  }
}
