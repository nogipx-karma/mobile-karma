FROM nogipx/flutter-web as flutter-web
RUN  mkdir /src
WORKDIR /src
COPY pubspec.yaml .
COPY lib ./lib
COPY web ./web
RUN  flutter build web

FROM nginx:latest as flutter-server
COPY --from=flutter-web /src/build/web /var/www/html
COPY deploy/nginx.conf /etc/nginx/conf.d/default.conf.template
EXPOSE 80 443
CMD /bin/bash -c "envsubst '\$PORT' < /etc/nginx/conf.d/default.conf.template > /etc/nginx/conf.d/default.conf" && nginx -g 'daemon off;'