// import 'package:flutter/material.dart';
// import 'package:flutter/rendering.dart';
// import 'package:flutter_google_ui/flutter_google_ui.dart';
// import 'package:karma/old_nullable/widget/export.dart';
// import 'package:flutter_cross_platform/flutter_cross_platform.dart';
// import 'package:karma/design/colors.dart';
// import 'package:splashscreen/splashscreen.dart';

// class WelcomeScreen extends StatelessWidget with ScreenSplitMixin {

//   @override
//   Widget build(BuildContext context) {
//     return SplashScreen(
//       seconds: 2,
//       navigateAfterSeconds: screenVariant(context),
//       backgroundColor: AppColor.jet,
//       styleTextUnderTheLoader: Theme.of(context).textTheme.headline6,
//       title: Text("KARMA \npersonal app",
//         textAlign: TextAlign.center,
//         style: Theme.of(context).primaryTextTheme.headline3.apply(
//           fontWeightDelta: 500,
//           color: AppColor.mintCream
//         )
//       ),
//       loadingText: Text("Please wait"),
//       loaderColor: AppColor.flame
//     );
//   }

//   @override
//   Widget desktop(context) => WelcomeScreenWeb();

//   @override
//   Widget mobile(context) => WelcomeScreenMobile();

// }

// class WelcomeScreenMobile extends StatelessWidget {

//   @override
//   Widget build(BuildContext context) {
//     return Scaffold(
//       drawer: Drawer(
//       ),
//       appBar: MyAppBar.get(context),
//       body: Container(
//         alignment: Alignment.center,
//         child: SingleChildScrollView(
//           padding: EdgeInsets.all(MaterialSpace.tiny),
//           child: Column(
//             crossAxisAlignment: CrossAxisAlignment.center,
//             mainAxisAlignment: MainAxisAlignment.spaceBetween,
//             children: [
//               Column(
//                 children: [
//                   WelcomeTitle(),
//                   WelcomeInfo(),
//                   WelcomeLinks(),
//                   WelcomeNavigation()
//                 ],
//               ),
//               Container(
//                 padding: EdgeInsets.all(MaterialSpace.tiny),
//                 child: CredentialsAtom.column()
//               )
//             ],
//           )
//         )
//       ),
//     );
//   }
// }

// class WelcomeScreenWeb extends StatelessWidget {

//   @override
//   Widget build(BuildContext context) {
//     return Scaffold(
//       drawer: Drawer(
//         elevation: 0,
//       ),
//       appBar: MyAppBar.get(context),
//       body: Container(
//         alignment: Alignment.center,
//         child: Column(
//           crossAxisAlignment: CrossAxisAlignment.start,
//           children: [
//             Expanded(
//               flex: 18,
//               child: Container(
//                 alignment: Alignment.center,
//                 child: Column(
//                   crossAxisAlignment: CrossAxisAlignment.center,
//                   mainAxisAlignment: MainAxisAlignment.center,
//                   children: [
//                     WelcomeTitle(),
//                     WelcomeInfo(),
//                     Padding(
//                       padding: GoogleInset.header_narrow,
//                       child: WelcomeLinks(),
//                     ),
//                     Padding(
//                       padding: EdgeInsets.symmetric(
//                         horizontal: 16, vertical: 48
//                       ),
//                       child: WelcomeNavigation(),
//                     ),
//                   ],
//                 ),
//               ),
//             ),
//             Expanded(
//               flex: 1,
//               child: Container(
//                 padding: EdgeInsets.all(MaterialSpace.tiny),
//                 child: CredentialsAtom.row()
//               )
//             )
//           ],
//         )
//       ),
//     );
//   }
// }
