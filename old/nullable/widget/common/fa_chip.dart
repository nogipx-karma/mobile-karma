import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_google_ui/flutter_google_ui.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

class FAChip extends StatelessWidget {

  final Color? color;
  final IconData? icon;
  final Widget? widgetIcon;
  final Widget? label;
  final Function? onTap;
  final double? iconSize;
  final double? borderRadius;
  final EdgeInsets? padding;
  final bool? dark;
  final double? hoverOpacity;
  final bool? hideEmptyIcon;

  const FAChip({
    this.color,
    this.icon,
    this.label,
    this.onTap,
    this.iconSize = 24,
    this.dark = false,
    this.borderRadius = MaterialRadius.card,
    this.padding,
    this.hideEmptyIcon = true,
    this.widgetIcon,
    this.hoverOpacity,
  }): assert(icon != null || label != null || widgetIcon != null,
        "At least one icon/label/widgetIcons should not be null"
      ),
      super();

  @override
  Widget build(BuildContext context) {
    final mainColor = color ?? Theme.of(context).accentColor;
    final chipPadding = padding ?? EdgeInsets.symmetric(
      vertical: MaterialSpace.tiny, horizontal: MaterialSpace.narrow
    );
    final hoverColor = hoverOpacity != null
      ? mainColor.withOpacity(hoverOpacity!)
      : dark! ? mainColor.hoverDark : mainColor.hover;
    return Material(
      clipBehavior: Clip.antiAlias,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(borderRadius!)
      ),
      type: MaterialType.transparency,
      child: InkWell(
        onTap: () => onTap!.call(),
        focusColor: dark! ? mainColor.activatedDark : mainColor.activated,
        hoverColor: hoverColor,
        splashColor: dark! ? mainColor.pressedDark : mainColor.pressed,
        highlightColor: dark! ? mainColor.activatedDark : mainColor.activated,
        child: Container(
          padding: chipPadding,
          child: Row(
            mainAxisSize: MainAxisSize.min,
            children: [
              Visibility(
                visible: (icon != null || widgetIcon != null) || !hideEmptyIcon!,
                child: Container(
                  width: 24,
                  margin: EdgeInsets.only(right: MaterialSpace.tiny),
                  child: LayoutBuilder(
                    builder: (context, _) {
                      if (icon != null)
                        return FaIcon(icon, size: iconSize, color: mainColor);

                      else if (icon == null && widgetIcon != null)
                        return widgetIcon!;

                      else
                        return Container();
                    },
                  )
                ),
              ),
              if (label != null)
                label!
            ],
          ),
        ),
      ),
    );
  }
}
