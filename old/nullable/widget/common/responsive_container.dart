import 'package:flutter/material.dart';
import 'package:responsive_builder/responsive_builder.dart';

class ResponsiveBody extends StatelessWidget {
  final Widget child;

  const ResponsiveBody({required this.child}) : super();
  
  @override
  Widget build(BuildContext context) {
    return Center(
      child: ResponsiveBuilder(
        builder: (context, sizing) {
          var width = MediaQuery.of(context)!.size.width * .95;

          if (sizing.isDesktop)
            width = MediaQuery.of(context)!.size.width * .6;
          
          if (sizing.isTablet)
            width = MediaQuery.of(context)!.size.width * .8;
          
          return Container(
            alignment: Alignment.topCenter,
            width: width,
            child: child,
          );
        },
      ),
    );
  }
}