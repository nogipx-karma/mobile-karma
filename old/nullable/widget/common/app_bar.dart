import 'package:flutter/material.dart';

class MyAppBar {
  MyAppBar._();
  
  static get (BuildContext context) {
    return AppBar(
      elevation: 0,
    );
  }
}