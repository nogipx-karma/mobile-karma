import 'package:flutter/material.dart';
import 'package:flutter_cross_platform/flutter_cross_platform.dart';
import 'package:flutter_google_ui/flutter_google_ui.dart';
import 'package:flutter_hover_handle/flutter_hover_handle.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:karma/design/extensions/alignment_extension.dart';
import 'package:responsive_builder/responsive_builder.dart';

class ScreenOverlayButtonWeb extends StatelessWidget {

  final Widget? icon;
  final Alignment? alignment;
  final Function? onTap;
  final Color? hoverColor;
  final Color? unselectedColor;
  final Curve? iconCurve;
  final Curve? bgCurve;

  ScreenOverlayButtonWeb({
    this.icon,
    this.alignment,
    this.onTap,
    this.hoverColor,
    this.unselectedColor,
    this.iconCurve = Curves.easeInOutQuart,
    this.bgCurve = Curves.easeInOutQuart,
  }) : super();

  @override
  Widget build(BuildContext context) {
    final mainColor = unselectedColor ?? Theme.of(context).accentColor;
    final hoverFactor = 1.3;
    final duration = Duration(milliseconds: 300);

    return ResponsiveBuilder(
      builder: (context, sizing) {
        final size = sizing.isDesktop || sizing.isTablet
          ? MediaQuery.of(context)!.size.height * .1
          : MediaQuery.of(context)!.size.width * .2;
        final hoverSize = size * hoverFactor;

        return HoverHandleBuilder(
          builder: (context, hovering) {
            Size? finalSize;
            final widthFactor = 1.8;
            final heightFactor = .65;

            if (hovering) {
              if (alignment!.isCorner)
                finalSize = Size(hoverSize, hoverSize);
              else if (alignment!.isSideVertical)
                finalSize = Size(hoverSize * heightFactor, hoverSize * widthFactor);
              else if (alignment!.isSideHorizontal)
                finalSize = Size(hoverSize * widthFactor, hoverSize * heightFactor);

            } else {
              if (alignment!.isCorner)
                finalSize = Size(size, size);
              else if (alignment!.isSideVertical)
                finalSize = Size(size * heightFactor, size * widthFactor);
              else if (alignment!.isSideHorizontal)
                finalSize = Size(size * widthFactor, size * heightFactor);
            }

            double? iconPadding;
            if (alignment!.isCorner || alignment!.isSideVertical)
              iconPadding = hovering
                ? finalSize!.width * .2 * hoverFactor
                : finalSize!.width * .25;

            else if (alignment!.isSideHorizontal)
              iconPadding = hovering
                ? finalSize!.height * .2 * hoverFactor
                : finalSize!.height * .25;

            final finalHoverColor = hovering
              ? hoverColor != null
                ? hoverColor
                : mainColor.withOpacity(.8)
              : mainColor;

            return Card(
              color: Colors.transparent,
              margin: EdgeInsets.all(0),
              shape: RoundedRectangleBorder(
                borderRadius: alignment!.toCornerBorderRadius(
                  radius: Radius.elliptical(hoverSize, hoverSize)
                ),
              ),
              child: AnimatedContainer(
                height: finalSize!.height,
                width: finalSize.width,
                duration: duration,
                curve: bgCurve!,
                color: finalHoverColor,
                child: InkWell(
                  focusColor: mainColor.activated,
                  splashColor: mainColor.pressed,
                  highlightColor: mainColor.selected,
                  hoverColor: mainColor.hover,
                  onTap: () => onTap!.call(),
                  child: Align(
                    alignment: alignment!,
                    child: AnimatedContainer(
                      duration: duration,
                      curve: iconCurve!,
                      padding: EdgeInsets.all(iconPadding!),
                      child: icon
                    ),
                  ),
                ),
              ),
            );
          },
        );
      },
    );
  }
}

class CornerButtonsOverlay
  extends StatelessWidget
  with PlatformSplitMixin {

  final Widget child;

  CornerButtonsOverlay({required this.child}) : super();

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: [
        child,
        platformVariant(context, data: {
          'icon': FontAwesomeIcons.idCard,
          'tip': "Info about me",
          'alignment': Alignment.topLeft,
          'onTap': () {}
        }),
        platformVariant(context, data: {
          'icon': FontAwesomeIcons.git,
          'tip': "Programming related",
          'alignment': Alignment.topRight,
          'onTap': () {}
        }),
        platformVariant(context, data: {
          'icon': FontAwesomeIcons.chevronCircleDown,
          'tip': "Learn more",
          'alignment': Alignment.bottomCenter,
          'onTap': () {}
        }),
      ],
    );
  }

  @override
  Widget device(context, {data}) {
    throw UnimplementedError();
  }

  @override
  Widget web(context, {data}) {
    final input = data as Map<String, dynamic>;
    return Positioned.fill(
      child: Align(
        alignment: input['alignment'],
        child: Tooltip(
          message: input['tip'],
          waitDuration: Duration(milliseconds: 500),
          textStyle: Theme.of(context).accentTextTheme.bodyText1.apply(
            fontWeightDelta: 500
          ),
          child: ScreenOverlayButtonWeb(
            hoverColor: Theme.of(context).accentColor.withOpacity(.8),
            unselectedColor: Theme.of(context).accentColor.withOpacity(.6),
            icon: ScreenTypeLayout(
              mobile: FaIcon(input['icon'], size: 24, color: Colors.white),
              tablet: FaIcon(input['icon'], size: 28, color: Colors.white),
              desktop: FaIcon(input['icon'], size: 32, color: Colors.white),
            ),
            alignment: input['alignment'],
            onTap: input['onTap']
          ),
        )
      ),
    );
  }
}
