import 'package:flutter/material.dart';
import 'package:flutter_google_ui/flutter_google_ui.dart';
import 'package:karma/data/outside_link.dart';
import 'package:karma/extensions/url_extension.dart';
import 'package:karma/design/colors.dart';
import 'package:responsive_builder/responsive_builder.dart';

import 'package:flutter_cross_platform/flutter_cross_platform.dart';

import '../export.dart';

class WelcomeLinks extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return ResponsiveBuilder(
      builder: (context, sizing) {
        final screenWidth = MediaQuery.of(context)!.size.width;
        return Container(
          padding: GoogleInset.header_narrow,
          width: sizing.isDesktop ? screenWidth * .5 : screenWidth,
          child: Wrap(
            spacing: MaterialSpace.tiny,
            runSpacing: MaterialSpace.tiny,
            alignment: WrapAlignment.center,
            children: [MySocials.gitlab, MySocials.telegram, MySocials.instagram, WorkRelated.hh, WorkRelated.codewars]
                .map((link) => FAChip(
                      icon: link.icon,
                      iconSize: 24,
                      label: Text(
                        link.name.split(" ")[0],
                        style: Theme.of(context).accentTextTheme.headline6.apply(fontWeightDelta: 500),
                      ),
                      hoverOpacity: .3,
                      color: link.color,
                      onTap: () => link.link!.launchUrl(),
                      dark: true,
                    ))
                .toList(),
          ),
        );
      },
    );
  }
}

class WelcomeInfo extends StatelessWidget with ScreenSplitMixin {
  final age = DateTime.now().difference(DateTime.parse("20001004")).inDays ~/ 365;

  @override
  Widget build(BuildContext context) {
    return screenVariant(context);
  }

  @override
  Widget desktop(context) => _buildText(
        textStyle: Theme.of(context).textTheme.headline4,
        age: age,
      );

  @override
  Widget mobile(context) => _buildText(textStyle: Theme.of(context).textTheme.headline6, age: age);

  Widget _buildText({required TextStyle textStyle, required age}) {
    return Container(
      padding: GoogleInset.header_narrow,
      child: Text("$age ${MydnessText.welcome_subtitle}", textAlign: TextAlign.center, style: textStyle),
    );
  }
}

class WelcomeTitle extends StatelessWidget with ScreenSplitMixin {
  @override
  Widget build(BuildContext context) {
    return screenVariant(context);
  }

  @override
  Widget desktop(context) {
    return _buildText(context, style: Theme.of(context).primaryTextTheme.headline1);
  }

  @override
  Widget mobile(context) {
    return _buildText(context, style: Theme.of(context).primaryTextTheme.headline2);
  }

  Widget _buildText(context, {required TextStyle style}) => Text(
        MydnessText.welcome_title,
        textAlign: TextAlign.center,
        style: style.copyWith(letterSpacing: 1.3, fontWeight: FontWeight.bold),
      );
}

class WelcomeNavigation extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      child: Wrap(
        spacing: 16,
        alignment: WrapAlignment.center,
        children: [
          NavigationButtonAtom(
            icon: Icon(Icons.pets, color: Colors.white),
            text: "My projects",
            onTap: () {
              // ExtendedNavigator.ofRouter<Router>().pushMyProjectsScreen();
            },
          ),
          NavigationButtonAtom(
            icon: Icon(Icons.work, color: Colors.white),
            text: "Work experience",
            onTap: () {
              // ExtendedNavigator.ofRouter<Router>().pushMyExperienceScreen();
            },
          ),
          NavigationButtonAtom(
            icon: Icon(Icons.person, color: Colors.white),
            text: "About me",
            onTap: () {
              // ExtendedNavigator.ofRouter<Router>().pushAboutMeScreen();
            },
          )
        ],
      ),
    );
  }
}
