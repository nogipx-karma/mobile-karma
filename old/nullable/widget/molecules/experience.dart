import 'package:flutter/material.dart';
import 'package:karma/design/colors.dart';
import 'package:karma/data/experience.dart';
import 'package:loggable_mixin/loggable_mixin.dart';

import '../export.dart';

class ExperienceMolecule extends StatelessWidget with Loggable {
  final Experience experience;

  ExperienceMolecule({required this.experience}) : super();

  @override
  Widget build(BuildContext context) {
    final hasProducts = experience.products != null && experience.products!.length > 0;
    return Container(
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Expanded(
            flex: 6,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                ExperienceTitleAtom(
                  name: experience.name,
                  logo: experience.logo,
                  website: experience.site,
                  logoBgColor: AppColor.mintCream,
                ),
                if (experience.from != null)
                  Padding(
                    padding: EdgeInsets.only(top: 12),
                    child: DateRangeAtom(
                      fromDate: experience.from,
                      toDate: experience.to,
                    ),
                  ),
              ],
            ),
          ),
          Expanded(
            flex: 13,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                _buildPosition(context, experience),
                Container(height: 16),
                ExperienceToolsAtom(title: "Languages", items: experience.languages!),
                ExperienceToolsAtom(title: "Tools", items: experience.tools!),
                if (hasProducts)
                  Divider(
                    height: 16,
                    thickness: 0.5,
                    color: AppColor.mintCream,
                  ),
                if (hasProducts)
                  Padding(
                    padding: EdgeInsets.only(top: 12),
                    child: Text("Projects",
                        style: Theme.of(context).textTheme.headline6.copyWith(fontWeight: FontWeight.bold)),
                  ),
                if (hasProducts)
                  ListView.separated(
                      shrinkWrap: true,
                      primary: false,
                      padding: EdgeInsets.only(top: 16),
                      itemCount: experience.products!.length,
                      separatorBuilder: (_, __) => Divider(height: 24, thickness: 0),
                      itemBuilder: (context, index) {
                        final project = experience.products![index];
                        return ExperienceProjectAtom(project: project);
                      }),
              ],
            ),
          )
        ],
      ),
    );
  }

  Widget _buildPosition(context, Experience experience) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Visibility(
          visible: experience.positionName != null,
          child: Text(experience.positionName,
              style: Theme.of(context).textTheme.headline5.copyWith(fontWeight: FontWeight.bold)),
        ),
        Visibility(
            visible: experience.positionDesc != null,
            child: Text(experience.positionDesc!, style: Theme.of(context).textTheme.bodyText1)),
      ],
    );
  }
}
