import 'package:flutter/material.dart';
import 'package:flutter_google_ui/flutter_google_ui.dart';
import 'package:karma/data/outside_link.dart';
import 'package:karma/extensions/url_extension.dart';

import '../export.dart';

class OutsideLinksWidget extends StatelessWidget {
  final String title;
  final List<OutsideLink> links;

  OutsideLinksWidget({required this.title, required this.links}) : super();

  @override
  Widget build(BuildContext context) {
    final List<OutsideLink> sorted = links.where((element) => element.icon != null).toList();
    sorted.addAll(links.where((element) => element.icon == null).toList());

    return GoogleSettingArea(
      title: title,
      child: ListView.separated(
        shrinkWrap: true,
        physics: NeverScrollableScrollPhysics(),
        padding: EdgeInsets.all(0),
        primary: false,
        itemCount: sorted.length,
        separatorBuilder: (_, __) => GoogleSettingDivider(),
        itemBuilder: (context, index) {
          final social = sorted[index];
          return _buildChip(context, social);
        },
      ),
    );
  }

  Widget _buildChip(context, OutsideLink social) {
    return FAChip(
      dark: true,
      icon: social.icon,
      iconSize: 20,
      label: Text(social.name, softWrap: true, style: Theme.of(context).textTheme.subtitle1),
      color: social.color,
      borderRadius: 0,
      hideEmptyIcon: false,
      onTap: () async {
        social.link!.launchUrl();
      },
      padding: GoogleInset.narrow,
    );
  }
}
