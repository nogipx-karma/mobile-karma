import 'package:flutter/material.dart';
import 'package:karma/extensions/url_extension.dart';

import '../export.dart';

enum _Type { Row, Column }

class CredentialsAtom extends StatelessWidget {
  final _Type _type;

  CredentialsAtom.row() : _type = _Type.Row;
  CredentialsAtom.column() : _type = _Type.Column;

  @override
  Widget build(BuildContext context) {
    if (_type == _Type.Row)
      return Row(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: _buildCredentials(context),
      );
    if (_type == _Type.Column)
      return Column(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: _buildCredentials(context),
      );
    throw UnimplementedError();
  }

  List<Widget> _buildCredentials(context) {
    return [
      FAChip(
        widgetIcon: FlutterLogo(),
        color: Color(0xff61d1fd),
        onTap: () => "https://flutter.dev/".launchUrl(),
        dark: true,
        label: Text(
          "powered with Flutter",
          style: Theme.of(context).accentTextTheme.bodyText2,
        ),
      ),
      Padding(
        padding: const EdgeInsets.all(8.0),
        child: Text(
          "nogipx@${DateTime.now().year}",
          style: Theme.of(context).accentTextTheme.bodyText2,
        ),
      ),
    ];
  }
}
