import 'package:flutter/material.dart';
import 'package:karma/design/colors.dart';

class NavigationButtonAtom extends StatelessWidget {
  final Widget icon;
  final String text;
  final Function onTap;

  const NavigationButtonAtom({ 
    required this.icon, required this.text, required this.onTap
  }): super();
  
  @override
  Widget build(BuildContext context) {
    return RaisedButton(
      color: AppColor.flame,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(16)
      ),
      onPressed: () => onTap(),
      child: Row(
        mainAxisSize: MainAxisSize.min,
        children: [
          Padding(
            padding: const EdgeInsets.only(right: 8),
            child: icon,
          ),
          Text(text,
            textAlign: TextAlign.center, 
            style: Theme.of(context).textTheme.subtitle1
          )
        ],
      ),
    );
  }
}