import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:intl/intl.dart';
import 'package:karma/design/colors.dart';
import 'package:karma/data/experience.dart';
import 'package:karma/data/outside_link.dart';
import 'package:karma/extensions/url_extension.dart';

import '../export.dart';

class ExperienceTitleAtom extends StatelessWidget {
  final String name;
  final Uri? logo;
  final Uri? website;
  final Uri? googlePlay;
  final Uri? appStore;
  final Icon? logoAlt;
  final Color? logoBgColor;
  final double logoSize;

  const ExperienceTitleAtom({
    required this.name,
    this.logo,
    this.website,
    this.logoAlt,
    this.logoBgColor = Colors.transparent,
    this.logoSize = 65,
    this.googlePlay,
    this.appStore,
  }) : super();

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Row(
        mainAxisSize: MainAxisSize.min,
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Container(
              clipBehavior: Clip.antiAlias,
              height: logoSize,
              width: logoSize,
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(12),
                color: logoBgColor,
              ),
              child: logo != null
                  ? Image.network(logo.toString(), fit: BoxFit.scaleDown, scale: 0.7)
                  : logoAlt ?? Icon(FontAwesomeIcons.heart, color: Colors.white, size: logoSize * 0.7)),
          Padding(
            padding: const EdgeInsets.symmetric(vertical: 2, horizontal: 16),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(name, style: Theme.of(context).textTheme.headline6.copyWith(fontWeight: FontWeight.bold)),
                if (website != null) _buildLink(context, "Website", website, FontAwesomeIcons.globe),
                if (googlePlay != null) _buildLink(context, "Google Play", googlePlay, FontAwesomeIcons.googlePlay),
                if (appStore != null) _buildLink(context, "App Store", appStore, FontAwesomeIcons.appStore)
              ],
            ),
          ),
        ],
      ),
    );
  }

  _buildLink(context, String title, Uri? link, IconData icon) {
    return RaisedButton(
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(12)),
      padding: EdgeInsets.symmetric(vertical: 0, horizontal: 8),
      onPressed: () {
        link.toString().launchUrl();
      },
      child: Row(
        children: [
          Padding(
            padding: const EdgeInsets.only(right: 8),
            child: Icon(
              icon,
              color: AppColor.mintCream,
              size: 16,
            ),
          ),
          Text(title, style: Theme.of(context).textTheme.caption)
        ],
      ),
    );
  }
}

class DateRangeAtom extends StatelessWidget {
  final DateTime? fromDate;
  final DateTime? toDate;

  const DateRangeAtom({this.fromDate, this.toDate}) : super();

  @override
  Widget build(BuildContext context) {
    final dateStyle = Theme.of(context).textTheme.subtitle1;
    final dateFormat = "dd MMMM yyyy";
    final captionStyle = Theme.of(context).textTheme.caption.apply(color: AppColor.platinum);
    const double captionWidth = 40;
    return Container(
      child: IntrinsicWidth(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Row(
              children: [
                Container(
                  width: captionWidth,
                  child: Text("From", style: captionStyle),
                ),
                Text(DateFormat(dateFormat).format(fromDate), style: dateStyle)
              ],
            ),
            Row(
              children: [
                Container(
                    width: captionWidth,
                    alignment: Alignment.center,
                    child: Icon(FontAwesomeIcons.caretDown, color: AppColor.flame, size: 16)),
                Builder(
                  builder: (context) {
                    final diff = (toDate ?? DateTime.now()).difference(fromDate ?? DateTime.now());
                    final years = diff.inDays ~/ 365;
                    final months = (diff.inDays ~/ 30) % 12;
                    final yearsResult = "$years years,";
                    final result = "${years > 0 ? yearsResult : ''} $months months".trim();
                    return Text(result,
                        style: captionStyle.copyWith(fontWeight: FontWeight.bold, color: AppColor.flame));
                  },
                ),
              ],
            ),
            Row(
              children: [
                Container(
                  width: captionWidth,
                  child: Text("To", style: captionStyle, textAlign: TextAlign.center),
                ),
                Builder(
                  builder: (context) {
                    if (toDate != null)
                      return Text(DateFormat(dateFormat).format(toDate), style: dateStyle);
                    else
                      return Text("Until now", style: dateStyle);
                  },
                )
              ],
            ),
          ],
        ),
      ),
    );
  }
}

class ExperienceProjectAtom extends StatelessWidget {
  final Experience project;

  const ExperienceProjectAtom({required this.project}) : super();

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Expanded(
            flex: 2,
            child: ExperienceTitleAtom(
                name: project.name,
                website: project.site,
                appStore: project.appStore,
                googlePlay: project.googlePlay,
                logo: project.logo,
                logoSize: 40,
                logoBgColor: AppColor.mintCream,
                logoAlt: Icon(FontAwesomeIcons.projectDiagram, color: AppColor.mintCream, size: 32)),
          ),
          Expanded(
            flex: 6,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(project.positionName, style: Theme.of(context).textTheme.headline6),
                Padding(
                  padding: const EdgeInsets.only(top: 8, bottom: 16),
                  child: Text(project.positionDesc!, style: Theme.of(context).textTheme.subtitle2),
                ),
                ExperienceToolsAtom(title: "Languages", items: project.languages!),
                ExperienceToolsAtom(title: "Tools", items: project.tools!)
              ],
            ),
          )
        ],
      ),
    );
  }
}

class ExperienceToolsAtom extends StatelessWidget {
  final List<OutsideLink> items;
  final String title;

  const ExperienceToolsAtom({required this.items, required this.title}) : super();

  @override
  Widget build(BuildContext context) {
    return Visibility(
      visible: items.isNotEmpty,
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Padding(
            padding: const EdgeInsets.only(right: 8, top: 5),
            child: Text(title, style: Theme.of(context).textTheme.bodyText1.copyWith(fontWeight: FontWeight.bold)),
          ),
          Expanded(
            child: Wrap(
              children: items
                  .map((e) => FAChip(
                        padding: EdgeInsets.all(6),
                        label: Text(e.name,
                            style: Theme.of(context).textTheme.bodyText2.copyWith(
                                // fontStyle: FontStyle.italic,
                                fontWeight: FontWeight.bold,
                                color: AppColor.flame)),
                        dark: true,
                      ))
                  .toList()
                  .cast<Widget>(),
            ),
          ),
        ],
      ),
    );
  }
}
