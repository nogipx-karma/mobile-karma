
import 'package:html/dom.dart';
import 'package:html/parser.dart';
import 'package:http/http.dart';

class AvitoRealtyAd {
  final int id;
  final String title;
  final List<Uri> photos;
  final String price;
  final String address;
  final Uri link;
  final String date;
  bool? isNew;

  AvitoRealtyAd({
    required this.id, this.isNew,
    required this.title, required this.photos, required this.price, 
    required this.address, required this.link, required this.date, 
  });

  factory AvitoRealtyAd.fromElement(Element e) {
    final error = "Parsing error";

    // Parse title
    final titleElem = e.querySelector(".snippet-link");
    final title = titleElem.attributes["title"];
    final link = Uri.parse("https://avito.ru/${titleElem.attributes['href']}");

    // Parse price 
    final priceElem = e.querySelector(".snippet-price-row span");
    final price = priceElem.text.trim().replaceAll('\n', '');

    // Parse address
    final addresElem = e.querySelector(".item-address__string");
    final address = addresElem.text.trim().replaceAll('\n', '');

    // Parse photos 
    final photosElems = e.querySelectorAll(".item-slider-image img");
    final photos = photosElems
      .map((e) {
        final srcset = e.attributes["data-srcset"] ?? e.attributes["srcset"] ?? '';
        final photo = srcset.split(" ")[0];
        return Uri.parse(photo);
      })
      .toList();

    // Parse date
    final dateElem = e.querySelector(".snippet-date-info");
    final date = dateElem.attributes["data-tooltip"] ?? error;

    return AvitoRealtyAd(
      id: int.tryParse(e.attributes["data-item-id"] ?? '') ?? 0,
      title: title ?? error,
      link: link,
      photos: photos,
      price: price,
      address: address,
      date: date,
    );
  }

  @override
  String toString() => "AvitoAd { id: $id, title: $title, date: $date, price: $price, photos: ${photos.length}}";
}

class AvitoAdsProvider {

  Client _http = Client();

  Future<Document> getPage(Uri searchUri) async {
    final response = await _http.get(searchUri).catchError((e) {
      print(e);
    });
    final html = parse(response.body);
    return html;
  }

}

extension AvitoDocument on Document {
  List<AvitoRealtyAd> extractAds() {
    final lines = this.querySelectorAll("[data-marker=catalog-serp] .snippet[id]");
    return lines.map((e) => AvitoRealtyAd.fromElement(e)).toList();
  }
}